#include "CheckPoint/MapEntrance.h"

#include "AuraGameModeBase.h"
#include "PlayerInterface.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AMapEntrance::AMapEntrance(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	Sphere->SetupAttachment(MoveToComponent);
}

void AMapEntrance::LoadActor_Implementation()
{
}

void AMapEntrance::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult& SweepResult)
{
	if (OtherActor->Implements<UPlayerInterface>())
	{
		bReached = true;

		IPlayerInterface::Execute_SaveProgress(OtherActor, DestinationPlayerStartTag,
		                                       DestinationMap.ToSoftObjectPath().GetAssetName());

		if (UGameplayStatics::GetGameMode(this))
			GetWorld()->ServerTravel(DestinationMap.ToSoftObjectPath().GetAssetName());
		// UGameplayStatics::OpenLevelBySoftObjectPtr(this, DestinationMap);
	}
}
