#include "Game/LoadScreenSaveGame.h"


bool ULoadScreenSaveGame::HasMap(const FString& InMapName, FSavedMap& OutSavedMap)
{
	for (const FSavedMap& Map : SavedMaps)
	{
		if (Map.MapAssetName == InMapName)
		{
			OutSavedMap = Map;
			return true;
		}
	}
	return false;
}
