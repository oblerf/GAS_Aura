#include "Actor/PointCollection.h"

#include "AuraAbilitySystemLibrary.h"

APointCollection::APointCollection()
{
	PrimaryActorTick.bCanEverTick = false;
}

FVector APointCollection::GetLandLocation(FVector PtLocation, TArray<AActor*> IgnoreActors, float ZValue) const
{
	const FVector RaisedLocation = FVector(PtLocation.X, PtLocation.Y, PtLocation.Z + ZValue);
	const FVector LoweredLocation = FVector(PtLocation.X, PtLocation.Y, PtLocation.Z - ZValue);

	FHitResult HitResult;

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActors(IgnoreActors);
	GetWorld()->LineTraceSingleByProfile(HitResult, RaisedLocation, LoweredLocation, FName("BlockAll"),
	                                     QueryParams);

	return HitResult.ImpactPoint;
}

TArray<USceneComponent*> APointCollection::GetGroundPoints(const FVector& GroundLocation, int32 NumPoints,
                                                           float ZValue, float radius)
{
	for (auto i = ImmutablePts.Num(); i < NumPoints; i++)
	{
		USceneComponent* Point = NewObject<USceneComponent>(
			this, USceneComponent::StaticClass(), FName(FString::Printf(TEXT("Pt_%s"), *FString::FromInt(i))));
		Point->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
		ImmutablePts.Add(Point);
	}

	TArray<AActor*> LivingActors;
	UAuraAbilitySystemLibrary::GetLivePlayersWithinRadius(this, LivingActors, TArray<AActor*>(), 200.f,
	                                                      GetActorLocation());
	auto CheckIndex = 0;
	for (; CheckIndex < LivingActors.Num() && CheckIndex < NumPoints; CheckIndex++)
	{
		const auto LandLocation = GetLandLocation(LivingActors[CheckIndex]->GetActorLocation(), LivingActors, ZValue);
		ImmutablePts[CheckIndex]->SetWorldLocation(LandLocation);
	}
	for (; CheckIndex < NumPoints; CheckIndex++)
	{
		const auto Rand2D = FMath::RandPointInCircle(radius);
		FVector CheckLocation = GroundLocation;
		CheckLocation.X += Rand2D.X;
		CheckLocation.Y += Rand2D.Y;
		const auto LandLocation = GetLandLocation(CheckLocation, LivingActors, ZValue);
		ImmutablePts[CheckIndex]->SetWorldLocation(LandLocation);
	}
	return ImmutablePts;
}

void APointCollection::BeginPlay()
{
	Super::BeginPlay();
}
