// 


#include "AI/AuraAIController.h"

#include "AI/AuraAIPerceptionComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"


// Sets default values
AAuraAIController::AAuraAIController()
{
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>("BlackboardComponent");
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>("BehaviorTreeComponent");
	AuraAIPerceptionComponent = CreateDefaultSubobject<UAuraAIPerceptionComponent>("AuraAIPerceptionComponent");
	SetPerceptionComponent(*AuraAIPerceptionComponent);
	check(Blackboard)
	check(BehaviorTreeComponent)
}
