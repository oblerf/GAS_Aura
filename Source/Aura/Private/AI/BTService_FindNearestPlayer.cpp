#include "AI/BTService_FindNearestPlayer.h"

#include "AIController.h"
#include "AuraCharacterBase.h"
#include "AI/AuraAIPerceptionComponent.h"
#include "BehaviorTree/BTFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"

void UBTService_FindNearestPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	if (!OwnerComp.GetAIOwner())
	{
		UKismetSystemLibrary::PrintString(GetWorld(),TEXT("GetAIOwner null"));
		return;
	}
	const auto AuraAIPerceptionComponent = OwnerComp.GetAIOwner()->FindComponentByClass<UAuraAIPerceptionComponent>();
	if (!AuraAIPerceptionComponent)return;

	float CloseDistance = 0.f;
	const auto CloseActor = AuraAIPerceptionComponent->GetClosestEnemy(CloseDistance);
	UBTFunctionLibrary::SetBlackboardValueAsObject(this, TargetToFollowSelector, CloseActor);
	if (CloseActor)
		UBTFunctionLibrary::SetBlackboardValueAsVector(this, TargetToFollowLocationSelector,
		                                               CloseActor->GetActorLocation());
	UBTFunctionLibrary::SetBlackboardValueAsFloat(this, DistanceToTargetSelector, CloseDistance);
}
