// 


#include "AI/AuraAIPerceptionComponent.h"

#include "AbilitySystemInterface.h"
#include "AIController.h"
#include "AuraAbilitySystemComponent.h"
#include "AuraAbilitySystemLibrary.h"
#include "AuraCharacterBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Sight.h"


AActor* UAuraAIPerceptionComponent::GetClosestEnemy(float& Distance) const
{
	TArray<AActor*> PerceiveActors;
	const auto OwnerController = Cast<AAIController>(GetOwner());
	if (!OwnerController)
	{
		UKismetSystemLibrary::PrintString(GetWorld(),TEXT("OwnerController is null"));
		return nullptr;
	}
	const auto OwnerPawn = OwnerController->GetPawn();

	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerceiveActors);
	if (PerceiveActors.Num() == 0)
	{
		GetCurrentlyPerceivedActors(UAISense_Damage::StaticClass(), PerceiveActors);
		if (PerceiveActors.Num() == 0)
			return nullptr;
	}

	float BestDistance = MAX_FLT;
	AActor* BestPawn = nullptr;
	for (const auto PerceiveActor : PerceiveActors)
	{
		const auto PerceiveCharacter = Cast<AAuraCharacterBase>(PerceiveActor);
		const auto AreEnemies = PerceiveCharacter &&
			UAuraAbilitySystemLibrary::AreEnemies(OwnerPawn, PerceiveCharacter);
		if (AreEnemies && !PerceiveCharacter->Execute_IsDead(PerceiveCharacter))
		{
			const auto CurrentDistance = (OwnerPawn->GetActorLocation() - PerceiveCharacter->GetActorLocation()).
				Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestPawn = PerceiveActor;
			}
		}
	}

	// UE_LOG(LogTemp, Display, TEXT("Num:%d  Name:%s"), PerceiveActors.Num(), *(BestPawn ? BestPawn->GetName() : "Nullptr"))
	Distance = BestDistance;
	return BestPawn;
}
