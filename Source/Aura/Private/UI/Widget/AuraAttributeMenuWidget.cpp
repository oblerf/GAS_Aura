#include "UI/Widget/AuraAttributeMenuWidget.h"

#include "Components/VerticalBox.h"
#include "Data/AttributeInfo.h"
#include "UI/Widget/AuraAttributeInfoWidget.h"
#include "UI/WidgetController/AttributeMenuWidgetController.h"

// void UAuraAttributeMenuWidget::SetPrimaryAttributeInfo(TArray<FAuraAttributeInfo> AttributeInfos)
// {
// 	PrimaryAttributeInfoBox->ClearChildren();
// 	PrimaryAttributeWidgets.Empty();
// 	for (const auto& It : AttributeInfos)
// 	{
// 		const auto AuraAttributeInfoWidget = CreateWidget<UAuraAttributeInfoWidget>(GetWorld(), AuraAttributeInfoClass);
// 		AuraAttributeInfoWidget->InitialAttributeInfo(It);
// 		PrimaryAttributeInfoBox->AddChild(AuraAttributeInfoWidget);
// 		PrimaryAttributeWidgets.Add(It.AttributeTag, AuraAttributeInfoWidget);
// 	}
// }
//
// void UAuraAttributeMenuWidget::SetSecondaryAttributeInfo(TArray<FAuraAttributeInfo> AttributeInfos)
// {
// 	SecondaryAttributeInfoBox->ClearChildren();
// 	SecondaryAttributeWidgets.Empty();
// 	for (const auto& It : AttributeInfos)
// 	{
// 		const auto AuraAttributeInfoWidget = CreateWidget<UAuraAttributeInfoWidget>(GetWorld(), AuraAttributeInfoClass);
// 		AuraAttributeInfoWidget->InitialAttributeInfo(It);
// 		SecondaryAttributeInfoBox->AddChild(AuraAttributeInfoWidget);
// 		SecondaryAttributeWidgets.Add(It.AttributeTag, AuraAttributeInfoWidget);
// 	}
// }

void UAuraAttributeMenuWidget::UpdateBoxesDataFromTag(UVerticalBox* InBox, FGameplayTag AttributeDataFilter,
                                                      TSubclassOf<UAuraAttributeInfoWidget> AuraAttributeInfoClass,
                                                      FAuraAttributeInfo Info)
{
	if (!Info.AttributeTag.MatchesTag(AttributeDataFilter))return;
	if (BoxesAttributeData.Contains(InBox) && BoxesAttributeData[InBox].AttributeWidgets.Contains(Info.AttributeTag))
	{
		const auto AuraAttributeInfoWidget = CreateWidget<UAuraAttributeInfoWidget>(
			BoxesAttributeData[InBox].AttributeWidgets[Info.AttributeTag]);
		AuraAttributeInfoWidget->InitialAttributeInfo(Info);
		return;
	}
	const auto AuraAttributeInfoWidget = CreateWidget<UAuraAttributeInfoWidget>(GetWorld(), AuraAttributeInfoClass);
	if (auto AttributeMenuWidgetController = CastChecked<UAttributeMenuWidgetController>(WidgetController))
	{
		AuraAttributeInfoWidget->OnUpgradeButtonClickSignature.AddLambda(
			[AttributeMenuWidgetController, Info](const FGameplayTag& AttributeTag)
			{
				AttributeMenuWidgetController->UpgradeAttribute(Info.AttributeTag);
			});
	}
	AuraAttributeInfoWidget->InitialAttributeInfo(Info);
	AuraAttributeInfoWidget->SetWidgetController(WidgetController);
	InBox->AddChild(AuraAttributeInfoWidget);
	if (!BoxesAttributeData.Contains(InBox))
		BoxesAttributeData.Add(InBox, FBoxAttributeCache(Info.AttributeTag, AuraAttributeInfoWidget));
	else
		BoxesAttributeData[InBox].AttributeWidgets.Add(Info.AttributeTag, AuraAttributeInfoWidget);
	// PrimaryAttributeWidgets.Add(Info.AttributeTag, AuraAttributeInfoWidget);
}
