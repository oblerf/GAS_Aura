#include "UI/Widget/AuraAttributeInfoWidget.h"

FAuraAttributeInfo UAuraAttributeInfoWidget::GetAttributeInfo() const
{
	return AttributeInfo;
}

void UAuraAttributeInfoWidget::InitialAttributeInfo(const FAuraAttributeInfo& NewAttributeInfo)
{
	AttributeInfo = NewAttributeInfo;
	// GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan,
	//                                  FString::Printf(
	// 	                                 TEXT("Tag: %s, Value: %f"), *NewAttributeInfo.AttributeTag.ToString(),
	// 	                                 NewAttributeInfo.AttributeValue));
	AttributeInfoSet();
}

void UAuraAttributeInfoWidget::BroadcastUpgradeButtonClick()
{
	OnUpgradeButtonClickSignature.Broadcast(AttributeInfo.AttributeTag);
}
