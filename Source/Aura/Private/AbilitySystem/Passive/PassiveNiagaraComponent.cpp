#include "AbilitySystem/Passive/PassiveNiagaraComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AuraAbilitySystemComponent.h"
#include "AuraGameplayTags.h"
#include "CombatInterface.h"

UPassiveNiagaraComponent::UPassiveNiagaraComponent()
{
	bAutoActivate = false;
}

void UPassiveNiagaraComponent::BeginPlay()
{
	Super::BeginPlay();

	if (UAuraAbilitySystemComponent* AuraASC = Cast<UAuraAbilitySystemComponent>(
		UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner())))
	{
		AuraASC->OnActivatePassiveEffect.AddUObject(this, &UPassiveNiagaraComponent::OnPassiveActivate);
		if (AuraASC->bStartupAbilitiesGiven && AuraASC->GetStatusFromAbilityTag(PassiveSpellTag) ==
			FAuraGameplayTags::Get().Abilities_Status_Equipped)
		{
			Activate();
		}
	}
	else if (ICombatInterface* CombatInterface = Cast<ICombatInterface>(GetOwner()))
	{
		CombatInterface->GetOnAscRegisteredDelegate().AddLambda([this](UAbilitySystemComponent* ASC)
		{
			if (UAuraAbilitySystemComponent* AuraASC = Cast<UAuraAbilitySystemComponent>(ASC))
			{
				AuraASC->OnActivatePassiveEffect.AddUObject(this, &UPassiveNiagaraComponent::OnPassiveActivate);
				if (AuraASC->bStartupAbilitiesGiven && AuraASC->GetStatusFromAbilityTag(PassiveSpellTag) ==
					FAuraGameplayTags::Get().Abilities_Status_Equipped)
				{
					Activate();
				}
			}
		});
	}
}

void UPassiveNiagaraComponent::OnPassiveActivate(const FGameplayTag& AbilityTag, bool bActivate)
{
	if (!PassiveSpellTag.MatchesTagExact(AbilityTag))return;
	if (bActivate && !IsActive())
	{
		Activate();
	}
	else if (!bActivate && IsActive())
	{
		Deactivate();
	}
}
