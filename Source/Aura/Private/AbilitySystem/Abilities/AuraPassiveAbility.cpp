// 


#include "Abilities/AuraPassiveAbility.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AuraAbilitySystemComponent.h"
#include "Kismet/KismetSystemLibrary.h"

void UAuraPassiveAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                          const FGameplayAbilityActorInfo* ActorInfo,
                                          const FGameplayAbilityActivationInfo ActivationInfo,
                                          const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	// UKismetSystemLibrary::PrintString(GetAvatarActorFromActorInfo(),
	//                                   FString::Printf(TEXT("ActivatePassiveAbility [%s]"), *GetNameSafe(this)));

	if (UAuraAbilitySystemComponent* AuraASC = Cast<UAuraAbilitySystemComponent>(
		UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetAvatarActorFromActorInfo())))
	{
		ApplyPassiveEffect();
		AuraASC->OnDeactivatePassiveAbility.AddUObject(this, &UAuraPassiveAbility::ReceiveDeactivate);
	}
}

void UAuraPassiveAbility::ReceiveDeactivate(const FGameplayTag& AbilityTag)
{
	if (AbilityTags.HasTagExact(AbilityTag))
	{
		UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetAvatarActorFromActorInfo())->
			RemoveActiveGameplayEffect(PassiveEffectHandle);

		// UKismetSystemLibrary::PrintString(GetAvatarActorFromActorInfo(),
		// 							  FString::Printf(TEXT("ReceivePassiveDeactivate [%s]"), *GetNameSafe(this)));
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}
}

FString UAuraPassiveAbility::GetDescription(int32 Level)
{
	FString Des = FString::Printf(TEXT(
		"<Title>%s</>\n\n"
		"<Small>Level: </><Level>%d</>\n\n"
	),
	                              *PassiveName,
	                              Level);
	if (PassiveGameplayEffectClass)
	{
		if (const UGameplayEffect* PassiveEffect = PassiveGameplayEffectClass->GetDefaultObject<UGameplayEffect>())
		{
			for (FGameplayModifierInfo Mod : PassiveEffect->Modifiers)
			{
				float ModMagnitude = 0.f;
				Mod.ModifierMagnitude.GetStaticMagnitudeIfPossible(Level, ModMagnitude);
				Des.Append(FString::Printf(
					TEXT("<Default>%s</> <Damage>%.1f</> <Percent>%s</>\n"),
					*UEnum::GetDisplayValueAsText(Mod.ModifierOp).ToString(),
					ModMagnitude,
					*Mod.Attribute.AttributeName
				));
			}
		}
	}

	return Des;
}

FString UAuraPassiveAbility::GetNextLevelDescription(int32 Level)
{
	return GetDescription(Level);
}

void UAuraPassiveAbility::ApplyPassiveEffect()
{
	AActor* SourceAvatarActor = GetAvatarActorFromActorInfo();
	UAuraAbilitySystemComponent* AuraASC = Cast<UAuraAbilitySystemComponent>(
		UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(SourceAvatarActor));

	FGameplayEffectContextHandle EffectContextHandle = AuraASC->MakeEffectContext();
	if (!PassiveGameplayEffectClass)return;
	EffectContextHandle.AddSourceObject(SourceAvatarActor);

	const FGameplayEffectSpecHandle SpecHandle = AuraASC->MakeOutgoingSpec(
		PassiveGameplayEffectClass, GetAbilityLevel(), EffectContextHandle);

	PassiveEffectHandle = AuraASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
}
