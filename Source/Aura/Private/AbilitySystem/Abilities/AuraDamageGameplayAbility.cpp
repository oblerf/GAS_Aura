// 


#include "Abilities/AuraDamageGameplayAbility.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "AuraGameplayTags.h"
#include "Aura/Aura.h"

UAuraDamageGameplayAbility::UAuraDamageGameplayAbility()
{
	ActivationBlockedTags.AddTag(FAuraGameplayTags::Get().Debuff_Stun);
}

void UAuraDamageGameplayAbility::CauseDamage(AActor* TargetActor)
{
	FGameplayEffectSpecHandle DamageSpecHandle = MakeOutgoingGameplayEffectSpec(DamageEffectClass, 1.f);
	for (auto Pair : DamageTypes)
	{
		const float ScaledDamage = Pair.Value.GetValueAtLevel(GetAbilityLevel());
		UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(DamageSpecHandle, Pair.Key, ScaledDamage);
	}
	GetAbilitySystemComponentFromActorInfo()->ApplyGameplayEffectSpecToTarget(
		*DamageSpecHandle.Data.Get(), UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(TargetActor));
}

FDamageEffectParams UAuraDamageGameplayAbility::MakeDamageEffectParamsFromClassDefaults(AActor* TargetActor) const
{
	FDamageEffectParams Params;
	Params.WorldContextObject = GetAvatarActorFromActorInfo();
	Params.DamageGameplayEffectClass = DamageEffectClass;
	Params.SourceAbilitySystemComponent = GetAbilitySystemComponentFromActorInfo();
	Params.TargetAbilitySystemComponent = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(TargetActor);
	TMap<FGameplayTag, float> InDamageTypes;
	const int32 InAbilityLevel = GetAbilityLevel();
	for (auto DamageType : DamageTypes)
	{
		InDamageTypes.Add(DamageType.Key, DamageType.Value.GetValueAtLevel(InAbilityLevel));
	}
	Params.DamageTypes = InDamageTypes;
	Params.AbilityLevel = InAbilityLevel;
	Params.DebuffChance = DebuffChance;
	Params.DebuffDamage = DebuffDamage;
	Params.DebuffDuration = DebuffDuration;
	Params.DebuffFrequency = DebuffFrequency;
	Params.DeathImpulseMagnitude = DeathImpulseMagnitude;
	Params.KnockBackChance = KnockBackChance;
	Params.KnockBackForceMagnitude = KnockBackForceMagnitude;
	if (IsValid(TargetActor))
	{
		FRotator Rotator = (TargetActor->GetActorLocation() - GetAvatarActorFromActorInfo()->GetActorLocation()).
			Rotation();
		Rotator.Pitch = CUSTOM_KNOCK_BACK_FORCE_PITCH;
		const FVector ToTarget = Rotator.Vector();
		Params.DeathImpulse = ToTarget * DeathImpulseMagnitude;
		const bool bIsKnockBackForce = FMath::RandRange(1, 100) < KnockBackChance;
		Params.KnockBackForce = bIsKnockBackForce ? ToTarget * KnockBackForceMagnitude : FVector::ZeroVector;
	}
	if (bIsRadialDamage)
	{
		Params.bIsRadialDamage = bIsRadialDamage;
		Params.RadialDamageInnerRadius = RadialDamageInnerRadius;
		Params.RadialDamageOuterRadius = RadialDamageOuterRadius;
		Params.RadialDamageOrigin = RadialDamageOrigin;
	}
	return Params;
}

float UAuraDamageGameplayAbility::GetDamageAtLevel()
{
	float Damage = 0.f;
	for (auto Element : DamageTypes)
	{
		Damage += Element.Value.GetValueAtLevel(GetAbilityLevel());
	}
	return Damage;
}

FTaggedMontage UAuraDamageGameplayAbility::GetRandomTaggedMontageFromArray(
	const TArray<FTaggedMontage>& TaggedMontages, int32& Selection) const
{
	if (TaggedMontages.Num() == 0)return FTaggedMontage();

	Selection = FMath::RandRange(0, TaggedMontages.Num() - 1);
	return TaggedMontages[Selection];
}

float UAuraDamageGameplayAbility::GetDamageByDamageType(float InLevel, const FGameplayTag& DamageType)
{
	checkf(DamageTypes.Contains(DamageType), TEXT("GameplayAbility %s does not contain DamageType %s"),
	       *GetNameSafe(this), *DamageType.ToString())
	return DamageTypes[DamageType].GetValueAtLevel(InLevel);
}
