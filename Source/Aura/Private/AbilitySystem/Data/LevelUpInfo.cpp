#include "AbilitySystem/Data/LevelUpInfo.h"

int32 ULevelUpInfo::FindLevelForXP(int32 XP) const
{
	auto Level = 1;
	for (; Level < LevelUpInformation.Num() && XP >= LevelUpInformation[Level].LevelUpRequirement; Level++)
	{
	}
	return Level;
}
