// 


#include "Data/LootTiers.h"

TArray<FLootItem> ULootTiers::GetLootItems()
{
	TArray<FLootItem> OutLootItems;

	for (FLootItem& Item : LootItems)
	{
		for (int32 i = 0; i < Item.MaxNumberToSpawn; i++)
		{
			if (FMath::FRandRange(1.f, 100.f) >= Item.ChanceToSpawn)continue;
			FLootItem NewItem;
			NewItem.LootClass = Item.LootClass;
			NewItem.bLootLevelOverride = Item.bLootLevelOverride;
			OutLootItems.Add(NewItem);
		}
	}

	return OutLootItems;
}
