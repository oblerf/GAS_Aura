#pragma once

#include "CoreMinimal.h"
#include "Data/AttributeInfo.h"
#include "UI/Widget/AuraUserWidget.h"
#include "AuraAttributeMenuWidget.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FBoxAttributeCache
{
	GENERATED_BODY()

	FBoxAttributeCache()
	{
	}

	FBoxAttributeCache(const FGameplayTag& Tag, UAuraAttributeInfoWidget* Widget)
	{
		AttributeWidgets.Add(Tag, Widget);
	}

	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	// FGameplayTag AttributeDataFilter;

	UPROPERTY(BlueprintReadOnly)
	TMap<FGameplayTag, UAuraAttributeInfoWidget*> AttributeWidgets;
};

USTRUCT(BlueprintType, Blueprintable)
struct FAttributeWidgetData
{
	GENERATED_BODY()
	FAttributeWidgetData()
	{
	}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag AttributeDataFilter;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UAuraAttributeInfoWidget> AuraAttributeInfoClass;
};

class UAuraAttributeInfoWidget;
class UVerticalBox;
/**
 * 
 */
UCLASS()
class AURA_API UAuraAttributeMenuWidget : public UAuraUserWidget
{
	GENERATED_BODY()

public:
	// void SetPrimaryAttributeInfo(TArray<FAuraAttributeInfo> AttributeInfos);
	// void SetSecondaryAttributeInfo(TArray<FAuraAttributeInfo> AttributeInfos);

protected:
	UPROPERTY(BlueprintReadOnly)
	TMap<UVerticalBox*, FBoxAttributeCache> BoxesAttributeData;

	UFUNCTION(BlueprintCallable)
	void UpdateBoxesDataFromTag(UVerticalBox* InBox, FGameplayTag AttributeDataFilter,
	                            TSubclassOf<UAuraAttributeInfoWidget> AuraAttributeInfoClass,
	                            FAuraAttributeInfo Info);

	// UPROPERTY(BlueprintReadWrite, meta=(BindWidget))
	// UVerticalBox* PrimaryAttributeInfoBox;
	// UPROPERTY(BlueprintReadWrite, meta=(BindWidget))
	// UVerticalBox* SecondaryAttributeInfoBox;
	//
	// UPROPERTY()
	// TMap<FGameplayTag, UUserWidget*> PrimaryAttributeWidgets;
	// UPROPERTY()
	// TMap<FGameplayTag, UUserWidget*> SecondaryAttributeWidgets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FAttributeWidgetData PrimaryAttributeWidgetData;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FAttributeWidgetData SecondaryAttributeWidgetData;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FAttributeWidgetData ResistanceAttributeWidgetData;
};
