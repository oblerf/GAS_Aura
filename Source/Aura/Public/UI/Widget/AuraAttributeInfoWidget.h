#pragma once

#include "CoreMinimal.h"
#include "Data/AttributeInfo.h"
#include "UI/Widget/AuraUserWidget.h"
#include "AuraAttributeInfoWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnUpgradeButtonClickSignature, const FGameplayTag& AttributeTag);


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class AURA_API UAuraAttributeInfoWidget : public UAuraUserWidget
{
	GENERATED_BODY()

public:
	FAuraAttributeInfo GetAttributeInfo() const;

	FOnUpgradeButtonClickSignature OnUpgradeButtonClickSignature;

	void InitialAttributeInfo(const FAuraAttributeInfo& NewAttributeInfo);

protected:
	UPROPERTY(BlueprintReadOnly)
	FAuraAttributeInfo AttributeInfo;

	UFUNCTION(BlueprintImplementableEvent)
	void AttributeInfoSet();

	UFUNCTION(BlueprintCallable)
	virtual void BroadcastUpgradeButtonClick();
};
