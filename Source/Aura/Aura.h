#pragma once

#include "CoreMinimal.h"

#define ECC_Projectile ECollisionChannel::ECC_GameTraceChannel1
#define ECC_Target ECollisionChannel::ECC_GameTraceChannel2
#define ECC_ExcludePlayer ECollisionChannel::ECC_GameTraceChannel3

//PP_Highlight.material
//ProjectSettings - Rendering - Postprocessing - Custom Depth-Stencil Pass: Enabled with Stencil
#define CUSTOM_DEPTH_RED 250
#define CUSTOM_DEPTH_BLUE 251
#define CUSTOM_DEPTH_TAN 252

#define CUSTOM_PROJECTILE_Z 50.f

#define CUSTOM_KNOCK_BACK_FORCE_PITCH 45.f
